package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.EmployeeDao;

/**
 * Servlet implementation class ConfirmRegist
 */
@WebServlet("/EmployeeConfirmRegist")
public class EmployeeConfirmRegist extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EmployeeConfirmRegist() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//【必須】文字化け防止
		request.setCharacterEncoding("UTF-8");

		//フォーム呼び出し
		String empId = request.getParameter("empId");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String age = request.getParameter("age");
		String adress = request.getParameter("adress");
		String tel = request.getParameter("tel");
		String birthStr = request.getParameter("birthStr");
		String departmentId = request.getParameter("departmentId");
		String startWork = request.getParameter("startWork");
		String closeWork = request.getParameter("closeWork");
		String entryDateStr = request.getParameter("entryDateStr");

		//データ挿入
		EmployeeDao empDao = new EmployeeDao();
		empDao.NewRegistration(empId, password, name, age, adress, tel, birthStr, departmentId, startWork,
				closeWork, entryDateStr);

		//結果画面へフォワード
		RequestDispatcher dispachar = request.getRequestDispatcher("/WEB-INF/jsp/employeeResult.jsp");
		dispachar.forward(request, response);

	}

}
