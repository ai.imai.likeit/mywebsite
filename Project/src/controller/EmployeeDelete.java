package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EmployeeDao;
import model.EmployeeBeans;

/**
 * Servlet implementation class EmployeeDelete
 */
@WebServlet("/EmployeeDelete")
public class EmployeeDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EmployeeDelete() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/*ログインセッション呼び出し*/
		HttpSession session = request.getSession();
		EmployeeBeans emp = (EmployeeBeans) session.getAttribute("empLogin");

		/*セッションがない場合、ログイン画面に戻る*/
		if (emp == null) {
			response.sendRedirect("Login");
			return;
		}

		/*GetパラメータとしてIDを受け取る*/
		String id = request.getParameter("id");

		//idよりデータ取得
		EmployeeDao empDao = new EmployeeDao();
		EmployeeBeans deleteEmp = empDao.idSearch(id);

		/*リクエストスコープに格納*/
		request.setAttribute("deleteEmp", deleteEmp);

		/*フォワード(→削除画面)*/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/employeeDelete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//【必須】文字化け防止
		request.setCharacterEncoding("UTF-8");

		//フォーム取得
		String id = request.getParameter("id");

		//データ削除
		EmployeeDao empDao = new EmployeeDao();
		empDao.employeeDelete(id);

		/*フォワード(→削除完了画面)*/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/employeeDeleteComplete.jsp");
		dispatcher.forward(request, response);

	}

}
