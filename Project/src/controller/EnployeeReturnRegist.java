package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DepartmentDao;
import model.DepartmentBeans;
import model.EmployeeBeans;

/**
 * Servlet implementation class EnployeeReturnRegist
 */
@WebServlet("/EnployeeReturnRegist")
public class EnployeeReturnRegist extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EnployeeReturnRegist() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//【必須】文字化け防止
		request.setCharacterEncoding("UTF-8");

		//フォーム呼び出し
		String empId = request.getParameter("empId");
		String password = request.getParameter("password");
		String empName = request.getParameter("name");
		String age=request.getParameter("age");
		String adress=request.getParameter("adress");
		String tel=request.getParameter("tel");
		String birth = request.getParameter("birthStr");
		String departmentId = request.getParameter("departmentId");
		String entryDate = request.getParameter("entryDateStr");

		//データ格納→リクエストスコープ格納
		EmployeeBeans empBeans = new EmployeeBeans(empId, password, empName,age,adress,tel, birth, departmentId, entryDate);
		request.setAttribute("empBeans", empBeans);

		//部署名格納
		DepartmentDao departmentDao = new DepartmentDao();
		List<DepartmentBeans> departmentBeansList = departmentDao.findAll();

		//リクエストスコープ格納
		request.setAttribute("departmentBeansList", departmentBeansList);

		//登録画面にフォワード
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/employeeNewRegist.jsp");
		dispatcher.forward(request, response);

	}

}
