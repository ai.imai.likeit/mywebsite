package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AttendanceManagementDao;
import model.AttendanceManagementBeans;
import model.EmployeeBeans;

/**
 * Servlet implementation class AttendCloseTWork
 */
@WebServlet("/AttendCloseWork")
public class AttendCloseWork extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AttendCloseWork() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/*【必須】文字化け防止*/
		request.setCharacterEncoding("UTF-8");

		//セッションにてログイン情報を取得→empid_no,startWork,closeWork取得
		HttpSession session = request.getSession();
		EmployeeBeans empLogin = (EmployeeBeans) session.getAttribute("empLogin");
		int empidNo = empLogin.getId();
		LocalTime startWork = empLogin.getStartWork();
		LocalTime closeWork = empLogin.getCloseWork();
		String today = empLogin.getLoginDateStr();

		//statusが1→退勤処理
		//statusが1以外→出勤処理不可
		AttendanceManagementDao attendDao = new AttendanceManagementDao();
		int statusNo = attendDao.findByStatus(empidNo, today);

		if (statusNo != 1) {
			response.sendRedirect("AttendanceManagement");
			return;
		}

		//勤務時間・昼休み・退勤時間・残長時間を計算
		//退勤：退勤ボタンを押した時間
		Calendar cl = Calendar.getInstance();
		LocalTime endTime = LocalTime.now();
		SimpleDateFormat sdfHM = new SimpleDateFormat("HH:mm:ss");
		String endTimeStr = sdfHM.format(cl.getTime());

		//勤怠時間：退勤-出勤-昼休み
		//出勤時間をDBより取得
		AttendanceManagementBeans attendBeans = attendDao.findStartTime(empidNo, today);
		LocalTime startTime = attendBeans.getStartTime();
		long attendMinutes;
		String lunchTime;
		String overTime;

		//出勤が09:00より早い→09:00出社扱い、昼休みあり
		//出勤が09:00よび遅い→出勤時間にて処理、昼休みなし
		if (startTime.isBefore(startWork)) {
			attendMinutes = ChronoUnit.MINUTES.between(startWork, endTime) - 60;
			lunchTime="01:00:00";
		} else {
			attendMinutes = ChronoUnit.MINUTES.between(startTime, endTime);
			lunchTime="00:00:00";
		}
		String attendTime = attendDao.timeConvert(attendMinutes);

		//残業時間：退勤-18:00
		if (endTime.isBefore(closeWork)) {
			//もし退勤＜18:00だったら→残業時間なし
			overTime = "00:00:00";
		} else {
			//もし退勤＜＝18:00だったら→残業時間あり
			long overMinutes = ChronoUnit.MINUTES.between( closeWork,endTime);
			overTime = attendDao.timeConvert(overMinutes);
		}
		//勤務時間・昼休み・退勤時間・残長時間をアップデート
		attendDao.updateEndTime(attendTime, lunchTime, endTimeStr, overTime, empidNo, today);

		//勤怠画面にリダイレクト
		response.sendRedirect("AttendanceManagement");
	}

}
