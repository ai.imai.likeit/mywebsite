package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DepartmentDao;
import dao.EmployeeDao;
import model.DepartmentBeans;
import model.EmployeeBeans;

/**
 * Servlet implementation class adminMenu
 */
@WebServlet("/AdminMenu")
public class AdminMenu extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminMenu() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/*ログインセッション呼び出し*/
		HttpSession session = request.getSession();
		EmployeeBeans emp = (EmployeeBeans) session.getAttribute("empLogin");

		/*セッションがない場合、ログイン画面に戻る*/
		if (emp == null) {
			response.sendRedirect("Login");
			return;
		}

		//部署名格納
		DepartmentDao departmentDao = new DepartmentDao();
		List<DepartmentBeans> departList = departmentDao.findAll();
		session.setAttribute("departList", departList);

		//メッセージ格納
		request.setAttribute("Msg", "何もないよ|_・)ﾁﾗｯ");

		/*セッションがある場合、新入社員登録へフォワード*/
		RequestDispatcher dispachar = request.getRequestDispatcher("/WEB-INF/jsp/adminMenu.jsp");
		dispachar.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//【必須】文字化け防止
		request.setCharacterEncoding("UTF-8");

		//フォームの取得
		String empId = request.getParameter("empId");
		String departId = request.getParameter("departId");
		String name = request.getParameter("name");

		if(empId.isEmpty() && departId.isEmpty() && name.isEmpty()) {
			//エラーメッセージ格納
			request.setAttribute("Msg", "検索値ないよ|_・)ﾁﾗｯ");

			//管理者メニュー画面へフォワード
			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/adminMenu.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//リスト取得
		EmployeeDao empDao = new EmployeeDao();
		List<EmployeeBeans> empList = empDao.findEmoloyeeList(empId, departId, name);

		if(empList.isEmpty()) {
			//エラーメッセージ格納
			request.setAttribute("Msg", "該当する人いないよ|_・)ﾁﾗｯ");

			//管理者メニュー画面へフォワード
			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/adminMenu.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//リクエストスコープに格納
		request.setAttribute("empList", empList);

		//管理者メニュー画面へフォワード
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/adminMenu.jsp");
		dispatcher.forward(request, response);

	}

}
