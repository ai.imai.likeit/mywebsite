package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EmployeeDao;
import model.EmployeeBeans;

/**
 * Servlet implementation class EmployeeUpdate
 */
@WebServlet("/EmployeeUpdate")
public class EmployeeUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EmployeeUpdate() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/*ログインセッション呼び出し*/
		HttpSession session = request.getSession();
		EmployeeBeans emp = (EmployeeBeans) session.getAttribute("empLogin");

		/*セッションがない場合、ログイン画面に戻る*/
		if (emp == null) {
			response.sendRedirect("Login");
			return;
		}

		/*GetパラメータとしてIDを受け取る*/
		String id = request.getParameter("id");

		//idよりデータ取得
		EmployeeDao empDao = new EmployeeDao();
		EmployeeBeans updateEmp = empDao.idSearch(id);

		/*リクエストスコープに格納*/
		request.setAttribute("updateEmp", updateEmp);

		/*フォワード(→修正画面)*/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/employeeUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//【必須】文字化け防止
		request.setCharacterEncoding("UTF-8");

		//フォーム呼び出し
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String adress = request.getParameter("adress");
		String tel = request.getParameter("tel");
		String departId = request.getParameter("departId");
		String startWork = request.getParameter("startWork");
		String closeWork = request.getParameter("closeWork");

		//アップデート
		EmployeeDao empDao=new EmployeeDao();
		empDao.employeeUpdate(id, name, tel, adress, departId, startWork, closeWork);

		/*フォワード(→修正完了画面)*/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/employeeUpdateComplete.jsp");
		dispatcher.forward(request, response);

	}

}
