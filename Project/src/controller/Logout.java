package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EmployeeDao;
import model.EmployeeBeans;

/**
 * Servlet implementation class Logout
 */
@WebServlet("/Logout")
public class Logout extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Logout() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//セッションよりempId, passwordを取得
		HttpSession session = request.getSession();
		EmployeeBeans empBeans = (EmployeeBeans) session.getAttribute("empLogin");
		String empId = empBeans.getEmpId();
		String password = empBeans.getPassword();

		//ログアウト時間をアップデート
		EmployeeDao empDao = new EmployeeDao();
		empDao.updateLogoutDate(empId, password);

		//セッション取得→削除*/
		session.removeAttribute("empLogin");
		session.removeAttribute("today");
		session.removeAttribute("departList");

		//ログイン画面に戻る*/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/logout.jsp");
		dispatcher.forward(request, response);
	}

}
