package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EmployeeDao;
import model.EmployeeBeans;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Login() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/*フォワード(→ログイン画面)*/
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/*【必須】文字化け防止*/
		request.setCharacterEncoding("UTF-8");

		/*ログイン画面に入力された値取得
		 * .getParameter("ログイン画面のinputで指定した値")*/
		String empId = request.getParameter("empId");
		String password = request.getParameter("password");

		/*引数セット、Dao実行*/
		EmployeeDao empDao = new EmployeeDao();
		EmployeeBeans empLogin = null;

		//ログイン時間をアップデート
		//パスワード判定(adminPassが暗号化されていない！)
		if (empId.equals("admin")) {
			empDao.updateLoginDateByAdmin(empId, password);
			empLogin = empDao.findByLoginIdAdmin(empId, password);

		} else {
			empDao.updateLoginDate(empId, password);
			empLogin = empDao.findByLoginId(empId, password);
		}

		/*ログイン失敗*/
		if (empLogin == null) {

			/*エラーメッセージ*/
			request.setAttribute("errMsg", "ログイン失敗Σ(･ロ･ﾉ)ﾉ");

			/*ログイン画面に戻る*/
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//ログイン成功*/
		String empLoginId = empLogin.getEmpId();

		//管理者がログイン*/
		if (empLoginId.equals("admin")) {

			//セッションにログイン情報をセット*/
			HttpSession session = request.getSession();
			session.setAttribute("empLogin", empLogin);

			//管理者画面にリダイレクト*/
			response.sendRedirect("AdminMenu");

		} else {

			//セッションにログイン情報をセット*/
			HttpSession session = request.getSession();
			session.setAttribute("empLogin", empLogin);

			/*勤怠画面にリダイレクト*/
			response.sendRedirect("AttendanceManagement");
		}

	}

}
