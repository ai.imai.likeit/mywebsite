package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AttendanceManagementDao;
import model.AttendanceManagementBeans;
import model.EmployeeBeans;

/**
 * Servlet implementation class AttendanceManagement
 */
@WebServlet("/AttendanceManagement")
public class AttendanceManagement extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AttendanceManagement() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/*【必須】文字化け防止*/
		request.setCharacterEncoding("UTF-8");

		//セッションスコープ設定→ログイン情報呼び出し
		HttpSession session = request.getSession();
		EmployeeBeans empLogin = (EmployeeBeans) session.getAttribute("empLogin");

		//セッションスコープが空の場合→ログイン画面にリダイレクト
		if (empLogin == null) {
			response.sendRedirect("Login");
			return;
		}

		//セッションよりログインID呼び出し
		int empidNo = empLogin.getId();
		int loginMonth = empLogin.getLoginDate().getMonthValue();

		//ログイン日付の当月を呼び出し
		Calendar cl = Calendar.getInstance();

		//判定
		//→ログインしたタイミングで月データが存在するか否か
		AttendanceManagementDao attendDao = new AttendanceManagementDao();
		int thisMonth = attendDao.findLoginMonth(empidNo, loginMonth);

		//存在しなかった場合
		if (thisMonth == 0) {

			// 月初日の計算
			int firstday = cl.getActualMinimum(Calendar.DAY_OF_MONTH);

			// 月末日の計算
			int lastday = cl.getActualMaximum(Calendar.DAY_OF_MONTH);

			//当月の日付生成
			for (int i = firstday; i <= lastday; i++) {
				//カレンダー生成→ 「i」を日付に設定
				Calendar calendar = Calendar.getInstance();
				calendar.set(Calendar.DATE, i);

				//必ず.getTime()！
				Date date = calendar.getTime();
				//→Wed May 01 18:03:21 JST 2019になる

				//形式の調整
				//日にち：yyyy-MM-dd、曜日：漢字
				SimpleDateFormat sdfYMD = new SimpleDateFormat("yyyy-MM-dd");
				String dateYMD = sdfYMD.format(date);
				SimpleDateFormat sdfE = new SimpleDateFormat("E");
				String dateE = sdfE.format(date);

				//Insert
				attendDao.makeCalender(empidNo, dateYMD, dateE);
			}
		}

		//合計値(勤務時間・ランチ時間、残業時間)の計算
		AttendanceManagementBeans totalTime = attendDao.calculationTotalTime(empidNo, loginMonth);
		request.setAttribute("totalTime", totalTime);

		//格納した月データを取得→リクエストスコープに格納
		List<AttendanceManagementBeans> calDefault = attendDao.findAll(empidNo, loginMonth);
		request.setAttribute("calDefault", calDefault);

		//statusNo取得→リクエストスコープに格納
		String today = empLogin.getLoginDateStr();
		int statusNo = attendDao.findByStatus(empidNo, today);
		request.setAttribute("statusNo", statusNo);

		//勤怠管理画面へフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/attendanceManagement.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
