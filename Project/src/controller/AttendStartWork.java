package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AttendanceManagementDao;
import model.EmployeeBeans;

/**
 * Servlet implementation class AttendStartWork
 */
@WebServlet("/AttendStartWork")
public class AttendStartWork extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AttendStartWork() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/*【必須】文字化け防止*/
		request.setCharacterEncoding("UTF-8");

		//sessionにてLogin情報を取得
		HttpSession session = request.getSession();
		EmployeeBeans empLogin = (EmployeeBeans) session.getAttribute("empLogin");

		//empid_no取得
		int empidNo = empLogin.getId();
		String today = empLogin.getLoginDateStr();

		//statusが0→出勤処理
		//statusが1以上→出勤処理不可
		AttendanceManagementDao attendDao = new AttendanceManagementDao();
		int statusNo = attendDao.findByStatus(empidNo, today);

		if (statusNo >= 1) {
			response.sendRedirect("AttendanceManagement");
			return;
		}

		//出勤の現在時刻を抽出
		Calendar cl = Calendar.getInstance();

		//出勤時間の形式を整える
		SimpleDateFormat sdfHMS = new SimpleDateFormat("HH:mm:ss");
		String startTime = sdfHMS.format(cl.getTime());

		//出勤時間をアップデート
		attendDao.updateStartTime(startTime, empidNo, today);

		//勤怠画面へリダイレクト
		response.sendRedirect("AttendanceManagement");

		//
		//
		//		//ログイン日付の当月を呼び出し
		//		int loginMonth = cl.get(Calendar.MONTH) + 1;
		//
		//		//格納した月データを取得→リクエストスコープに格納
		//		List<AttendanceManagementBeans> calUpDate = attendDao.findAll(empidNo, loginMonth);
		//		request.setAttribute("calUpDate", calUpDate);
		//
		//		//勤怠画面にフォワード
		//		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/attendanceManagement.jsp");
		//		dispatcher.forward(request, response);

	}

}
