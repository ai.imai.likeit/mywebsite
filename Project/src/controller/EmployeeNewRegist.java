package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EmployeeDao;
import model.EmployeeBeans;

/**
 * Servlet implementation class
 */
@WebServlet("/EmployeeNewRegist")
public class EmployeeNewRegist extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EmployeeNewRegist() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/*ログインセッション呼び出し*/
		HttpSession session = request.getSession();
		EmployeeBeans emp = (EmployeeBeans) session.getAttribute("empLogin");

		/*セッションがない場合、ログイン画面に戻る*/
		if (emp == null) {
			response.sendRedirect("Login");
			return;
		}

		/*セッションがある場合、新入社員登録へフォワード*/
		RequestDispatcher dispachar = request.getRequestDispatcher("/WEB-INF/jsp/employeeNewRegist.jsp");
		dispachar.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//【必須】文字化け防止
		request.setCharacterEncoding("UTF-8");

		//フォームの値呼び出し
		String empId = request.getParameter("empId");
		String password = request.getParameter("empPass");
		String name = request.getParameter("empName");
		String adress = request.getParameter("empAdress");
		String tel = request.getParameter("empTel");
		String birthDateStr = request.getParameter("empBirthDate");
		String departId = request.getParameter("departId");
		String entryDateStr = request.getParameter("empEntryDate");

		//エラー判定
		//・空がある場合(未入力)
		if (empId.isEmpty() || password.isEmpty() || name.isEmpty() || adress.isEmpty()
				|| tel.isEmpty() || birthDateStr.isEmpty() || departId.isEmpty() || entryDateStr.isEmpty()) {

			//リクエストスコープ格納
			request.setAttribute("errMsg", "未入力あるよΣ('ロ';)？！");

			//同画面にフォワード
			RequestDispatcher dispachar = request.getRequestDispatcher("/WEB-INF/jsp/employeeNewRegist.jsp");
			dispachar.forward(request, response);
			return;
		}

		//年齢計算
		EmployeeDao emoDao = new EmployeeDao();
		int age = emoDao.calcuratAge(birthDateStr);

		//入力成功の場合→インスタンスに格納
		EmployeeBeans empNewRegist = new EmployeeBeans(empId, password, name, age, adress, tel, birthDateStr,
				departId, entryDateStr);

		//リクエストスコープに格納→フォワード
		request.setAttribute("empNewRegist", empNewRegist);

		RequestDispatcher dispachar = request.getRequestDispatcher("/WEB-INF/jsp/employeeConfirmRegist.jsp");
		dispachar.forward(request, response);

	}

}
