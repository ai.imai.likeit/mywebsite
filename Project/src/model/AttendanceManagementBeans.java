package model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class AttendanceManagementBeans {

	private int id;
	private int empidNo;
	private Date days;
	private String dayOfWeek;
	private String kind;
	private LocalTime attendTime;
	private LocalTime startTime;
	private LocalTime lunchTime;
	private LocalTime endTime;
	private LocalTime overTime;
	private int status;

	private String totalAttendTime;
	private String totalLunchTime;
	private String totalOverTime;

	public AttendanceManagementBeans(String startTime) {
		setStartTimeStr(startTime);
	}

	public AttendanceManagementBeans(String totalAttendTime, String totalLunchTime,
			String totalOverTime) {

		this.totalAttendTime = totalAttendTime;
		this.totalLunchTime = totalLunchTime;
		this.totalOverTime = totalOverTime;
	}

	public AttendanceManagementBeans(int id, int empidNo, String days, String dayofweek,
			String kind, String attendTime, String startTime, String lunchTime, String endTime,
			String overTime) {
		this.id = id;
		this.empidNo = empidNo;
		setDaysStr(days);
		this.dayOfWeek = dayofweek;
		this.kind = kind;
		setAttendTimeStr(attendTime);
		setStartTimeStr(startTime);
		setLunchTimeStr(lunchTime);
		setEndTimeStr(endTime);
		setOverTimeStr(overTime);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEmpidNo() {
		return empidNo;
	}

	public void setEmpidNo(int empidNo) {
		this.empidNo = empidNo;
	}

	public Date getDays() {
		return days;
	}

	public void setDays(Date days) {
		this.days = days;
	}

	//DaysのString型
	public String getDaysStr() {
		//Date→String
		String str = new SimpleDateFormat("yyyy-MM-dd").format(this.days);
		return str;
	}

	//DaysのString型(dのみ)
	public String getDaysStrD() {
		//Date→String
		String str = new SimpleDateFormat("dd").format(this.days);
		return str;
	}

	//DaysのString型
	public void setDaysStr(String daysStr) {
		//String→Date
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		// Date型変換
		Date formatDate = null;
		try {
			formatDate = sdf.parse(daysStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		this.days = formatDate;
	}

	public String getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(String dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public LocalTime getAttendTime() {
		return attendTime;
	}

	public void setAttendTime(LocalTime attendTime) {
		this.attendTime = attendTime;
	}

	//AttendTimeのString型
	public String getAttendTimeStr() {
		//Localtime→String
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
		String timeS = dtf.format(attendTime);
		return timeS;
	}

	//AttendTimeのString型
	public void setAttendTimeStr(String attendTime) {
		//String→Localtime
		LocalTime time = LocalTime.parse(attendTime, DateTimeFormatter.ofPattern("HH:mm:ss"));
		this.attendTime = time;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	//StartTimeのString型
	public String getStartTimeStr() {
		//Localtime→String
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
		String timeS = dtf.format(startTime);
		return timeS;
	}

	//StartTimeのString型
	public void setStartTimeStr(String startTime) {
		//String→Localtime
		LocalTime time = LocalTime.parse(startTime, DateTimeFormatter.ofPattern("HH:mm:ss"));
		this.startTime = time;
	}

	public LocalTime getLunchTime() {
		return lunchTime;
	}

	public void setLunchTime(LocalTime lunchTime) {
		this.lunchTime = lunchTime;
	}

	//LunchTimeのString型
	public String getLunchTimeStr() {
		//Localtime→String
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
		String timeS = dtf.format(lunchTime);
		return timeS;
	}

	//LunchTimeのString型
	public void setLunchTimeStr(String lunchTime) {
		//String→Localtime
		LocalTime time = LocalTime.parse(lunchTime, DateTimeFormatter.ofPattern("HH:mm:ss"));
		this.lunchTime = time;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	//endTimeのString型
	public String getEndTimeStr() {
		//Localtime→String
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
		String timeS = dtf.format(endTime);
		return timeS;
	}

	//endTimeのString型
	public void setEndTimeStr(String endTime) {
		//String→Localtime
		LocalTime time = LocalTime.parse(endTime, DateTimeFormatter.ofPattern("HH:mm:ss"));
		this.endTime = time;
	}

	public LocalTime getOverTime() {
		return overTime;
	}

	public void setOverTime(LocalTime overTime) {
		this.overTime = overTime;
	}

	//OverTimeのString型
	public String getOverTimeStr() {
		//Localtime→String
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
		String timeS = dtf.format(overTime);
		return timeS;
	}

	//OverTimeのString型
	public void setOverTimeStr(String overTime) {
		//String→Localtime
		LocalTime time = LocalTime.parse(overTime, DateTimeFormatter.ofPattern("HH:mm:ss"));
		this.overTime = time;
	}

	public String getTotalAttendTime() {
		return totalAttendTime;
	}

	public void setTotalAttendTime(String totalAttendTime) {
		this.totalAttendTime = totalAttendTime;
	}

	public String getTotalLunchTime() {
		return totalLunchTime;
	}

	public void setTotalLunchTime(String totalLunchTime) {
		this.totalLunchTime = totalLunchTime;
	}

	public String getTotalOverTime() {
		return totalOverTime;
	}

	public void setTotalOverTime(String totalOverTime) {
		this.totalOverTime = totalOverTime;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
