package model;

public class DepartmentBeans {

	private int id;
	private String name;

	public DepartmentBeans() {

	}

	public DepartmentBeans(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {

	}

	public String getIdStr() {
		//int→String
		String s = String.valueOf(this.id);
		return s;
	}

	public int setIdStr(String id) {
		//String→int
		int i = Integer.parseInt(id);
		return i;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
