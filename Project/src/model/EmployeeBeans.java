package model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import dao.DepartmentDao;

public class EmployeeBeans {

	private int id;
	private String empId;
	private String password;
	private String name;
	private int age;
	private String adress;
	private String tel;
	private Date birth;
	private int departmentId;
	private LocalTime startWork;
	private LocalTime closeWork;
	private Date entryDate;
	private LocalDate loginDate;

	private String depName;

	public EmployeeBeans() {

	}

	public EmployeeBeans(String empId, String password) {
		this.empId = empId;
		this.password = password;
	}

	public EmployeeBeans(int id, String empId, String password) {
		this.id = id;
		this.empId = empId;
		this.password = password;
	}

	public EmployeeBeans(String id, String empId, String depName, String name) {
		setIdStr(id);
		this.empId = empId;
		this.depName = depName;
		this.name = name;
	}

	public EmployeeBeans(int id, String empId, String password, String name, String startWork, String closeWork,
			String loginDate) {
		this.id = id;
		this.empId = empId;
		this.password = password;
		this.name = name;
		setStartWorkStr(startWork);
		setCloseWorkStr(closeWork);
		setLoginDateStr(loginDate);
	}

	//EmployeeNewRegist-doPost
	public EmployeeBeans(String empId, String password, String name, int age, String adress,
			String tel, String birthStr, String department, String entryDateStr) {
		this.empId = empId;
		this.password = password;
		this.name = name;
		this.age = age;
		this.adress = adress;
		this.tel = tel;
		setBirthStr(birthStr);
		setDepartmentIdStr(department);
		setEntryDateStr(entryDateStr);
	}

	//EmployeeConfirmRegist-doPost
	public EmployeeBeans(String empId, String password, String name, String age, String adress, String tel,
			String birthStr, String department, String entryDateStr) {
		this.empId = empId;
		this.password = password;
		this.name = name;
		setAgeStr(age);
		this.adress = adress;
		this.tel = tel;
		setBirthStr(birthStr);
		setDepartmentIdStr(department);
		setEntryDateStr(entryDateStr);
	}

	//EmployeeDelete-deGet
	public EmployeeBeans(String id, String empId, String password, String name, String age, String adress, String tel,
			String birthStr, String department, String startWorkStr, String closeWorkStr, String entryDateStr) {

		setIdStr(id);
		this.empId = empId;
		this.password = password;
		this.name = name;
		setAgeStr(age);
		this.adress = adress;
		this.tel = tel;
		setBirthStr(birthStr);
		setDepartmentIdStr(department);
		setStartWorkStr(startWorkStr);
		setCloseWorkStr(closeWorkStr);
		setEntryDateStr(entryDateStr);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIdStr() {
		//int→String
		String s = String.valueOf(this.id);
		return s;
	}

	public void setIdStr(String id) {
		//String→int
		int i = Integer.parseInt(id);
		this.id = i;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getAgeStr() {
		//int→String
		String s = String.valueOf(this.age);
		return s + "歳";
	}

	public void setAgeStr(String age) {
		//String→int
		int i = Integer.parseInt(age);
		this.age = i;
	}


	public String getAdress() {
		return adress;
	}

	public String getTel() {
		return tel;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}

	//	birth:String型用
	public String getBirthStr() {
		//Date→String
		String str = new SimpleDateFormat("yyyy-MM-dd").format(this.birth);
		return str;
	}

	public String getBirthStrJap() {
		//Date→String
		String str = new SimpleDateFormat("yyyy年MM月dd日").format(this.birth);
		return str;
	}

	public void setBirthStr(String birthStr) {
		//String→Date
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		// Date型変換
		Date formatDate = null;
		try {
			formatDate = sdf.parse(birthStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		this.birth = formatDate;
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public String getDepartmentName() {
		DepartmentDao dao = new DepartmentDao();
		return dao.findDepartmentName(departmentId).getName();
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	public String getDepartmentIdStr() {
		//int→String
		String s = String.valueOf(this.departmentId);
		return s;
	}

	public void setDepartmentIdStr(String departmentId) {
		//String→int
		int i = Integer.parseInt(departmentId);
		this.departmentId = i;
	}

	public LocalTime getStartWork() {
		return startWork;
	}

	public void setStartWork(LocalTime startWork) {
		this.startWork = startWork;
	}

	public String getStartWorkStr() {
		//LocalTime→String
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
		String timeS = dtf.format(this.startWork);
		return timeS;
	}

	public void setStartWorkStr(String startWork) {
		//String→Localtime
		LocalTime time = LocalTime.parse(startWork, DateTimeFormatter.ofPattern("HH:mm:ss"));
		this.startWork = time;
	}

	public LocalTime getCloseWork() {
		return closeWork;
	}

	public void setCloseWork(LocalTime closeWork) {
		this.closeWork = closeWork;
	}

	public String getCloseWorkStr() {
		//LocalTime→String
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
		String timeS = dtf.format(this.closeWork);
		return timeS;
	}

	public void setCloseWorkStr(String closeWork) {
		//String→Localtime
		LocalTime time = LocalTime.parse(closeWork, DateTimeFormatter.ofPattern("HH:mm:ss"));
		this.closeWork = time;
	}

	public Date getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	//	entryDate:String型用
	public String getEntryDateStr() {
		//Date→String
		String str = new SimpleDateFormat("yyyy-MM-dd").format(this.entryDate);
		return str;
	}

	public String getEntryDateStrJap() {
		//Date→String
		String str = new SimpleDateFormat("yyyy年MM月dd日").format(this.entryDate);
		return str;
	}

	public void setEntryDateStr(String entryDateStr) {
		//String→Date
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		// Date型変換
		Date formatDate = null;
		try {
			formatDate = sdf.parse(entryDateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		this.entryDate = formatDate;
	}

	public LocalDate getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(LocalDate loginDate) {
		this.loginDate = loginDate;
	}

	public String getLoginDateStr() {
		//LocalDate→String;
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String login = dtf.format(this.loginDate);
		return login;
	}

	public String getLoginDateStrJap() {
		//LocalDate→String;
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy年MM月dd日");
		String login = dtf.format(this.loginDate);
		return login;
	}

	public String getLoginDateStrM() {
		//LocalDate→String;
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM");
		String login = dtf.format(this.loginDate);
		return login;
	}

	public void setLoginDateStr(String loginDate) {
		//String→LocalDate
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate login = LocalDate.parse(loginDate, dtf);
		this.loginDate = login;
	}

	public String getDepName() {
		return depName;
	}

	public void setDepName(String depName) {
		this.depName = depName;
	}

}
