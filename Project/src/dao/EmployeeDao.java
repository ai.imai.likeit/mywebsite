package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import database.DBManager;
import model.EmployeeBeans;

public class EmployeeDao {

	public void updateLoginDateByAdmin(String empId, String password) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();
			String sqlUpdate = "update employee "
					+ "set login_date = now() "
					+ "where emp_id = ? and password = ?";

			PreparedStatement pS = conn.prepareStatement(sqlUpdate);
			pS.setString(1, empId);
			pS.setString(2, password);
			pS.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void updateLoginDate(String empId, String password) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();
			String sqlUpdate = "update employee "
					+ "set login_date = now() "
					+ "where emp_id = ? and password = ?";

			PreparedStatement pS = conn.prepareStatement(sqlUpdate);
			pS.setString(1, empId);
			pS.setString(2, convertPasswordToEncrypotion(password));
			pS.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void updateLogoutDate(String empId, String password) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();
			String sqlUpdate = "update employee "
					+ "set logout_date = now() "
					+ "where emp_id = ? and password = ?";

			PreparedStatement pS = conn.prepareStatement(sqlUpdate);
			pS.setString(1, empId);
			pS.setString(2, password);
			pS.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}


	public EmployeeBeans findByLoginIdAdmin(String empId, String password) {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "select "
					+ "id,"
					+ "emp_id as empId,"
					+ "password,"
					+ "name,"
					+ "start_work as startWork,"
					+ "close_work as closeWork,"
					+ "Cast(login_date as DATE) as loginDate "
					+ "from employee "
					+ "where emp_id = ? and password = ?";

			PreparedStatement pstml = conn.prepareStatement(sql);
			pstml.setString(1, empId);
			pstml.setString(2, password);
			ResultSet rs = pstml.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int idData = rs.getInt("id");
			String empIdData = rs.getString("empId");
			String passwordData = rs.getString("password");
			String nameData = rs.getString("name");
			String startWorkData = rs.getString("startWork");
			String closeWorkData = rs.getString("closeWork");
			String loginDateData = rs.getString("loginDate");

			//LocalTime st = rs.getTime("st").toLocalTime();

			return new EmployeeBeans(idData, empIdData, passwordData, nameData, startWorkData, closeWorkData,
					loginDateData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public EmployeeBeans findByLoginId(String empId, String password) {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "select "
					+ "id,"
					+ "emp_id as empId,"
					+ "password,"
					+ "name,"
					+ "start_work as startWork,"
					+ "close_work as closeWork,"
					+ "Cast(login_date as DATE) as loginDate  "
					+ "from employee "
					+ "where emp_id = ? and password = ?";

			PreparedStatement pstml = conn.prepareStatement(sql);
			pstml.setString(1, empId);
			pstml.setString(2, convertPasswordToEncrypotion(password));
			ResultSet rs = pstml.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int idData = rs.getInt("id");
			String empIdData = rs.getString("empId");
			String passwordData = rs.getString("password");
			String nameData = rs.getString("name");
			String startWorkData = rs.getString("startWork");
			String closeWorkData = rs.getString("closeWork");
			String loginDateData = rs.getString("loginDate");

			return new EmployeeBeans(idData, empIdData, passwordData, nameData, startWorkData, closeWorkData,
					loginDateData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/*入力したパスワードを暗号化する*/
	public String convertPasswordToEncrypotion(String pass) {

		String source = pass;
		Charset charset = StandardCharsets.UTF_8;
		String algorithm = "MD5";
		byte[] bytes = null;

		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		return result;

	}

	public void NewRegistration(String empId, String password, String name, String age, String adress,
			String tel, String birthStr, String departmentId, String startWorkStr, String closeWorkStr,
			String entryDateStr) {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			/*INSERT文セット*/
			String sqlInsert = "INSERT INTO employee" +
					"(emp_id,"
					+ "password,"
					+ "name,"
					+ "age,"
					+ "adress,"
					+ "tel,"
					+ "birth,"
					+ "department,"
					+ "start_work,"
					+ "close_work,"
					+ "entry_date,"
					+ "registration_date,"
					+ "login_date) "
					+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,now(),now())";

			PreparedStatement pSInsert = conn.prepareStatement(sqlInsert);
			pSInsert.setString(1, empId);
			pSInsert.setString(2, convertPasswordToEncrypotion(password));
			pSInsert.setString(3, name);
			pSInsert.setString(4, age);
			pSInsert.setString(5, adress);
			pSInsert.setString(6, tel);
			pSInsert.setString(7, birthStr);
			pSInsert.setString(8, departmentId);
			pSInsert.setString(9, startWorkStr);
			pSInsert.setString(10, closeWorkStr);
			pSInsert.setString(11, entryDateStr);

			/*INSERT結果格納*/
			pSInsert.executeUpdate();

			/*入力チェック系はサーブレットに明記する*/

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public List<EmployeeBeans> findEmoloyeeList(String empId, String depId, String name) {

		Connection conn = null;
		List<EmployeeBeans> empList = new ArrayList<>();

		try {

			conn = DBManager.getConnection();
			String sql = "select"
					+ " id,"
					+ "emp_id as empId,"
					+ "name,"
					+ "dep_name as depName"
					+ " from employee e"
					+ " inner join department d"
					+ " on e.department = d.dep_id"
					+ " where emp_id != 'admin'";

			if (!(empId.isEmpty())) {
				sql += " and emp_id like '%" + empId + "%'";
			}
			if (!(depId.isEmpty())) {
				sql += " and department = '" + depId + "'";
			}
			if ((!name.isEmpty())) {
				sql += " and name like '%" + name + "%'";
			}

			sql += " order by id asc";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				String idData = rs.getString("id");
				String empIdData = rs.getString("empId");
				String depNameData = rs.getString("depName");
				String nameData = rs.getString("name");

				EmployeeBeans empB = new EmployeeBeans(idData, empIdData, depNameData, nameData);

				empList.add(empB);
			}
			return empList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public int calcuratAge(String birth) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

		// 生年月日を表す文字列から、LocalDateを生成
		LocalDate localBirdhdate = LocalDate.parse(birth, formatter);

		// 現在の日付を取得
		LocalDate nowDate = LocalDate.now();

		// 現在と生年月日の差分を年単位で算出することによって、年齢を計算する
		long age = ChronoUnit.YEARS.between(localBirdhdate, nowDate);

		return (int) age;
	}

	public EmployeeBeans idSearch(String id) {

		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "select"
					+ " id,"
					+ "emp_id as empId,"
					+ "password,"
					+ "name,"
					+ "age,"
					+ "adress,"
					+ "tel,"
					+ "birth,"
					+ "department as depId,"
					+ "start_work as startWork,"
					+ "close_work as closeWork,"
					+ "entry_date as entryDate"
					+ " from employee"
					+ " where id = ?";

			PreparedStatement pS = conn.prepareStatement(sql);
			pS.setString(1, id);
			ResultSet rs = pS.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String idData = rs.getString("id");
			String empIdData = rs.getString("empId");
			String passData = rs.getString("password");
			String nameData = rs.getString("name");
			String ageData = rs.getString("age");
			String adressData = rs.getString("adress");
			String telData = rs.getString("tel");
			String birthData = rs.getString("birth");
			String depIdData = rs.getString("depId");
			String startWorkData = rs.getString("startWork");
			String closeWorkData = rs.getString("closeWork");
			String entryDateData = rs.getString("entryDate");

			return new EmployeeBeans(idData, empIdData, passData, nameData, ageData, adressData, telData,
					birthData, depIdData, startWorkData, closeWorkData, entryDateData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void employeeDelete(String id) {

		Connection conn = null;
		try {

			conn = DBManager.getConnection();
			String sqlDelete = "delete from employee"
					+ " where id = ?;";

			PreparedStatement pS = conn.prepareStatement(sqlDelete);
			pS.setString(1, id);
			pS.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void employeeUpdate(String id, String name, String tel, String adress, String departId, String startWork,
			String closeWork) {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sqlUpdate = "update employee"
					+ " set name = ?,"
					+ "adress = ?,"
					+ "tel = ?,"
					+ "department = ?,"
					+ "start_work = ?,"
					+ "close_work = ?,"
					+ "update_date = now()"
					+ " where id = ?";

			PreparedStatement pS = conn.prepareStatement(sqlUpdate);
			pS.setString(1, name);
			pS.setString(2, adress);
			pS.setString(3, tel);
			pS.setString(4, departId);
			pS.setString(5, startWork);
			pS.setString(6, closeWork);
			pS.setString(7, id);

			pS.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}
}