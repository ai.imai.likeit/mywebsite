package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import database.DBManager;
import model.AttendanceManagementBeans;

public class AttendanceManagementDao {

	//ログインした日時の月データがあるかないか
	public int findLoginMonth(int empidNo, int loginMonth) {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String sql = "SELECT "
					+ "count(*) as count "
					+ "FROM attendance "
					+ "WHERE empid_no = ? and MONTH(days) >= ?";

			PreparedStatement pS = conn.prepareStatement(sql);
			pS.setInt(1, empidNo);
			pS.setInt(2, loginMonth);
			ResultSet rs = pS.executeQuery();

			if (!rs.next()) {
				//nullは文字列のみ使用可能→データがない場合「0」とする
				return 0;
			}

			//検索結果を取得
			int c = rs.getInt("count");
			return c;

		} catch (SQLException e) {
			e.printStackTrace();
			//nullは文字列のみ使用可能→データがない場合「0」とする
			return 0;

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	//カレンダー生成：employee_id,days,dayofweek
	public void makeCalender(int empidNo, String days, String dayofweek) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sqiInsert = "Insert into attendance"
					+ "(empid_no,days,dayofweeks) "
					+ "values(?,?,?)";

			PreparedStatement pSInsert = conn.prepareStatement(sqiInsert);
			pSInsert.setInt(1, empidNo);
			pSInsert.setString(2, days);
			pSInsert.setString(3, dayofweek);

			pSInsert.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public List<AttendanceManagementBeans> findAll(int empidNo, int month) {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			List<AttendanceManagementBeans> calDefault = new ArrayList<>();

			String sql = "select "
					+ "id,"
					+ "empid_no as empidNo,"
					+ "days,"
					+ "dayofweeks,"
					+ "kind,"
					+ "attend_time as attendTime,"
					+ "start_time as startTime,"
					+ "lunch_time as lunchTime,"
					+ "end_time as endTime,"
					+ "over_time as overTime "
					+ "from attendance "
					+ "where empid_no = ? and MONTH(days) = ? "
					+ "order by id asc";

			PreparedStatement pS = conn.prepareStatement(sql);
			pS.setInt(1, empidNo);
			pS.setInt(2, month);
			ResultSet rs = pS.executeQuery();

			while (rs.next()) {
				int idData = rs.getInt("id");
				int empidNoData = rs.getInt("empidNo");
				String daysData = rs.getString("days");
				String dayofweekData = rs.getString("dayofweeks");
				String kindData = rs.getString("kind");
				String attendTimeData = rs.getString("attendTime");
				String startTimeData = rs.getString("startTime");
				String lunchTimeData = rs.getString("lunchTime");
				String endTimeData = rs.getString("endTime");
				String overTimeData = rs.getString("overTime");

				AttendanceManagementBeans attendMB = new AttendanceManagementBeans(idData, empidNoData, daysData,
						dayofweekData, kindData, attendTimeData, startTimeData, lunchTimeData, endTimeData,
						overTimeData);

				calDefault.add(attendMB);
			}
			return calDefault;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//出勤時間インサート
	public void updateStartTime(String startTime, int empidNo, String today) {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sqlUpdate = "update attendance "
					+ "set start_time = ? , status = 1 "
					+ "where empid_no = ? and days = ?";

			PreparedStatement pS = conn.prepareStatement(sqlUpdate);
			pS.setString(1, startTime);
			pS.setInt(2, empidNo);
			pS.setString(3, today);
			pS.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			if (conn != null) {

				try {
					conn.close();

				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//出勤時間を取得
	public AttendanceManagementBeans findStartTime(int empidNo, String today) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();
			String sql = "select start_time as startTime "
					+ "from attendance "
					+ "where empid_no = ? and days = ?";

			PreparedStatement pS = conn.prepareStatement(sql);
			pS.setInt(1, empidNo);
			pS.setString(2, today);
			ResultSet rs = pS.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String startTimeData = rs.getString("startTime");
			return new AttendanceManagementBeans(startTimeData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public void updateEndTime(String attendTime, String lunchTime, String endTime,
			String overTime, int empidNo, String today) {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String splUpdate = "update attendance "
					+ "set "
					+ "attend_time = ?,"
					+ "lunch_time = ?,"
					+ "end_time = ?,"
					+ "over_time = ?,"
					+ "status = 2 "
					+ "where empid_no = ? and days = ?";

			PreparedStatement pSupdate = conn.prepareStatement(splUpdate);
			pSupdate.setString(1, attendTime);
			pSupdate.setString(2, lunchTime);
			pSupdate.setString(3, endTime);
			pSupdate.setString(4, overTime);
			pSupdate.setInt(5, empidNo);
			pSupdate.setString(6, today);

			pSupdate.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public String timeConvert(long timeDifference) {

		int minutes = (int) timeDifference;
		int min = 0;
		int hour = 0;

		hour = minutes / 60;
		min = minutes % 60;

		LocalTime localTime = LocalTime.of(hour, min);

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
		String time = dtf.format(localTime);

		return time;
	}

	public AttendanceManagementBeans calculationTotalTime(int empidNo, int month) {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String sql = "select "
					+ "sec_to_time(sum(time_to_sec(attend_time))) as totalAttendTime,"
					+ "sec_to_time(sum(time_to_sec(lunch_time))) as totalLunchTime,"
					+ "sec_to_time(sum(time_to_sec(over_time))) as totalOverTime "
					+ "from attendance "
					+ "where empid_no = ? and MONTH(days) = ?";

			PreparedStatement pS = conn.prepareStatement(sql);
			pS.setInt(1, empidNo);
			pS.setInt(2, month);
			ResultSet rs = pS.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String totalAT = rs.getString("totalAttendTime");
			int endIndexAT = totalAT.lastIndexOf(':');
			String totalAttendTimeData = totalAT.substring(0, endIndexAT);

			String totalLT = rs.getString("totalLunchTime");
			int endIndexLT = totalLT.lastIndexOf(':');
			String totalLunchTimeData = totalLT.substring(0, endIndexLT);

			String totalOT = rs.getString("totalOverTime");
			int endIndexOT = totalOT.lastIndexOf(':');
			String totalOverTimeData = totalOT.substring(0, endIndexOT);

			return new AttendanceManagementBeans(totalAttendTimeData, totalLunchTimeData, totalOverTimeData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public int findByStatus(int empidNo, String today) {

		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "select status "
					+ "from attendance "
					+ "where empid_no = ? and days = ?";

			PreparedStatement pS = conn.prepareStatement(sql);
			pS.setInt(1, empidNo);
			pS.setString(2, today);
			ResultSet rs = pS.executeQuery();

			if (!rs.next()) {
				return 0;
			}

			int statusNo = rs.getInt("status");
			return statusNo;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}
}
