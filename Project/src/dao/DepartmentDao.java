package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import database.DBManager;
import model.DepartmentBeans;

public class DepartmentDao {

	public DepartmentBeans findDepartmentName(int id) {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT "
					+ "dep_id as depId,"
					+ "dep_name as depName "
					+ "FROM department "
					+ "WHERE dep_id = ?";

			PreparedStatement pS = conn.prepareStatement(sql);
			pS.setInt(1, id);
			ResultSet rs = pS.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int depIdDate = rs.getInt("depId");
			String depNameDate = rs.getString("depName");

			return new DepartmentBeans(depIdDate,depNameDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			try {
				conn.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public List<DepartmentBeans> findAll() {

		List<DepartmentBeans> departmentBeans = new ArrayList<>();

		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT "
					+ "dep_id as depId,"
					+ "dep_name as depName "
					+ "FROM department "
					+ "order by dep_id asc";

			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {
				int depIdData = rs.getInt("depId");
				String depNameData = rs.getString("depName");
				departmentBeans.add(new DepartmentBeans(depIdData,depNameData));
			}
			return departmentBeans;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			try {
				conn.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
