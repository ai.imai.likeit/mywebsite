<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="Stylesheet" href="css/common.css">
<link rel="Stylesheet" href="css/employeeConfirmRegist.css">
<title>確認画面</title>
</head>
<body>
	<div class="header"></div>
	<div class="center header-text-area">
		<div class="row">
			<div class="col-sm-5">${today}</div>
			<div class="col-sm-2 header-form-area-line">
				やっほー今日もお疲れ様<br> <span class="textBold">${empLogin.name}様</span>(*^^)旦★
			</div>
			<div class="col-sm-5"></div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm header-form-area-line">入力に間違いはないかな？</div>
		</div>
		<div class="row">
			<div class="col-sm">
				<h3>さぁ確認しよう！</h3>
			</div>
		</div>
	</div>
	<form action="EmployeeConfirmRegist" method="post">
		<div class="center comfirm-form-area">
			<div>
				<input type="hidden" name="empId" value="${empNewRegist.empId}">
				<input type="hidden" name="name" value="${empNewRegist.name}">
			</div>
			<div class="row comfirm-form-area-line">
				<div class="col-sm-3 right">社員ID</div>
				<div class="col-sm-3 left">${empNewRegist.empId}</div>
				<div class="col-sm-3 right">社員名</div>
				<div class="col-sm-3 left">${empNewRegist.name}</div>
			</div>
			<div>
				<input type="hidden" name="password" value="${empNewRegist.password}">
				<input type="hidden" name="adress" value="${empNewRegist.adress}">
			</div>
			<div class="row comfirm-form-area-line">
				<div class="col-sm-3 right">パスワード</div>
				<div class="col-sm-3 left">${empNewRegist.password}</div>
				<div class="col-sm-3 right">住所</div>
				<div class="col-sm-3 left">${empNewRegist.adress}</div>
			</div>
			<div>
				<input type="hidden" name="departmentId" value="${empNewRegist.departmentId}">
				<input type="hidden" name="tel" value="${empNewRegist.tel}">
			</div>
			<div class="row comfirm-form-area-line">
				<div class="col-sm-3 right">部署名</div>
				<div class="col-sm-3 left">${empNewRegist.departmentName}</div>
				<div class="col-sm-3 right">電話番号</div>
				<div class="col-sm-3 left">${empNewRegist.tel}</div>
			</div>
			<div>
				<input type="hidden" name="entryDateStr" value="${empNewRegist.entryDateStr}">
				<input type="hidden" name="birthStr" value="${empNewRegist.birthStr}">
			</div>
			<div class="row comfirm-form-area-line">
				<div class="col-sm-3 right">入社年月日</div>
				<div class="col-sm-3 left">${empNewRegist.entryDateStrJap}</div>
				<div class="col-sm-3 right">生年月日</div>
				<div class="col-sm-3 left">${empNewRegist.birthStrJap}</div>
			</div>

			<div>
				<input type="hidden" name="startWork" value="09:00:00">
				<input type="hidden" name="age" value="${empNewRegist.age}">
			</div>
			<div class="row comfirm-form-area-line">
				<div class="col-sm-3 right">始業時間</div>
				<div class="col-sm-3 left">09:00</div>
				<div class="col-sm-3 right">年齢</div>
				<div class="col-sm-3 left">${empNewRegist.ageStr}</div>
			</div>
			<div>
				<input type="hidden" name="closeWork" value="18:00:00">
			</div>
			<div class="row comfirm-form-area-line">
				<div class="col-sm-3 right">終業時間</div>
				<div class="col-sm-3 left">18:00</div>
				<div class="col-sm-3 right"></div>
				<div class="col-sm-3 left"></div>
			</div>
			<div class="row comfirm-form-area-line">
				<div class="col-sm">
					<input class="signUpBtn" type="submit" value="登録">
				</div>
			</div>
		</div>
	</form>

	<form action="EnployeeReturnRegist" method="post">
		<div class="row center">
			<div class="col-sm">間違えちゃってる…</div>
		</div>
		<div>
			<input type="hidden" name="empId" value="${empNewRegist.empId}">
			<input type="hidden" name="password" value="${empNewRegist.password}">
			<input type="hidden" name="name" value="${empNewRegist.name}">
			<input type="hidden" name="age" value="${empNewRegist.age}">
			<input type="hidden" name="adress" value="${empNewRegist.adress}">
			<input type="hidden" name="tel" value="${empNewRegist.tel}">
			<input type="hidden" name="birthStr" value="${empNewRegist.birthStr}">
			<input type="hidden" name="departmentId" value="${empNewRegist.departmentId}">
			<input type="hidden" name="entryDateStr" value="${empNewRegist.entryDateStr}">
		</div>
		<div class="row center">
			<div class="col-sm">
				<input class="returnBtn" type="submit" value="戻る"
					onclick="location.href='EmployeeNewRegist'">
			</div>
		</div>
	</form>
	<div class="center footer-text-area"></div>
	<div class="footerLong"></div>
</body>
</html>