<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="Stylesheet" href="css/common.css">
<link rel="Stylesheet" href="css/logout.css">
<title>ログアウト</title>
</head>
<body>
	<div class="header"></div>
	<div class="header-text-area"></div>
	<div class="logout-form-area">
		<h2 class="style">ログアウトしました</h2>
		<br>
		<p class="center">
			<input class="returnBtnLarge" type="button" value="ログイン画面へ"
			onclick="location.href='Login'">
		</p>
	</div>
	<div class="footer"></div>
</body>
</html>