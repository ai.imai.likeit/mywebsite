<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="Stylesheet" href="css/common.css">
<link rel="Stylesheet" href="css/attendanceManagement.css">
<title>勤怠管理</title>
</head>
<body>
	<div class="header"></div>
	<div class="center header-text-area">
		<div class="row">
			<div class="col-sm-5">${empLogin.loginDateStrJap}</div>
			<div class="col-sm-2">
				やっほー今日もお疲れ様<br> <span class="textBold">${empLogin.name}様</span>(*^^)旦★
			</div>
			<div class="col-sm-5">
				<a href="Logout">ログアウト</a>
			</div>
		</div>
	</div>
	<div class="center attendance-form-area">
		<div class="row">
			<div class="col-sm">
				<c:choose>
					<c:when test="${statusNo == 0}">
						<h5>
							<span class="textBold">忘れずに勤怠管理！</span> <br>出勤ボタンを押そう|・_・)ﾁﾗｯ
						</h5>
					</c:when>
					<c:when test="${statusNo == 1}">
						<h5>
							<span class="textBold">只今出勤中！</span> <br>退勤ボタン忘れずに|・_・)ﾁﾗｯ
						</h5>
					</c:when>
					<c:when test="${statusNo == 2}">
						<h5>一日お疲れ様でした(*˘v˘*)ｽﾔｧ</h5>
					</c:when>
				</c:choose>
			</div>
		</div>
		<div class="row attendance-form-area-line">
			<div class="col-sm textBold">
				【${empLogin.startWorkStr}】-【${empLogin.closeWorkStr}】</div>
		</div>
		<div class="row attendance-form-area-line">
			<div class="col-sm-6 right">
				<form action="AttendStartWork" method="post">
					<c:if test="${statusNo == 0}">
						<input class="goingBtn" type="submit" value="出勤">
					</c:if>
					<c:if test="${statusNo >= 1}">
						<input class="goingBtn" type="submit" value="出勤" disabled>
					</c:if>
				</form>
			</div>
			<div class="col-sm-6 left">
				<form action="AttendCloseWork" method="post">
					<c:if test="${statusNo != 1}">
						<input class="leavingBtn" type="submit" value="退勤" disabled>
					</c:if>
					<c:if test="${statusNo == 1}">
						<input class="leavingBtn" type="submit" value="退勤">
					</c:if>
				</form>
			</div>
		</div>
	</div>
	<form action="" method="post">
		<div class="center calender-form-area">
			<div class="row calender-heading">
				<div class="col-sm-1">日</div>
				<div class="col-sm-1">曜日</div>
				<div class="col-sm-1">種類</div>
				<div class="col-sm">勤務時間</div>
				<div class="col-sm">出勤</div>
				<div class="col-sm">昼休み</div>
				<div class="col-sm">退勤</div>
				<div class="col-sm">残業</div>
			</div>

			<c:forEach var="cal" items="${calDefault}">
				<c:choose>
					<c:when test="${cal.daysStr == empLogin.loginDateStr}">
						<div class="row calender-line-today">
					</c:when>
					<c:when test="${cal.dayOfWeek == '土'}">
						<div class="row calender-line-sat">
					</c:when>
					<c:when test="${cal.dayOfWeek == '日'}">
						<div class="row calender-line-sun">
					</c:when>
					<c:otherwise>
						<div class="row calender-line">
					</c:otherwise>
				</c:choose>
				<div class="col-sm-1">${cal.daysStrD}</div>
				<div class="col-sm-1">${cal.dayOfWeek}</div>
				<div class="col-sm-1">${cal.kind}</div>
				<div class="col-sm">${cal.attendTimeStr}</div>
				<div class="col-sm">${cal.startTimeStr}</div>
				<div class="col-sm">${cal.lunchTimeStr}</div>
				<div class="col-sm">${cal.endTimeStr}</div>
				<div class="col-sm">${cal.overTimeStr}</div>
		</div>
		</c:forEach>
		<div class="row calender-total">
			<div class="col-sm-1"></div>
			<div class="col-sm-1"></div>
			<div class="col-sm-1"></div>
			<div class="col-sm">${totalTime.totalAttendTime}</div>
			<div class="col-sm"></div>
			<div class="col-sm">${totalTime.totalLunchTime}</div>
			<div class="col-sm"></div>
			<div class="col-sm">${totalTime.totalOverTime}</div>
		</div>
		</div>
		<div>
			<p class="center">
				<input class="submissionBtn" type="submit" value="月末提出だよ！">
			</p>
		</div>
	</form>
	<div class="footerLong"></div>
</body>
</html>