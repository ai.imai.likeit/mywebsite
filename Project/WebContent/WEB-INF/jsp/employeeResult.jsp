<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="Stylesheet" href="css/common.css">
<link rel="Stylesheet" href="css/employeeResult.css">
<title>登録結果</title>
</head>
<body>
	<div class="header"></div>
	<div class="center header-text-area">
		<div class="row">
			<div class="col-sm-5">${empLogin.loginDateStrJap}</div>
			<div class="col-sm-2 header-form-area-line">
				やっほー今日もお疲れ様<br> <span class="textBold">${empLogin.name}様</span>(*^^)旦★
			</div>
			<div class="col-sm-5">
				<a href="Logout">ログアウト</a>
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm center">
			<h2>入力完了</h2>
			<p class="textSmall">( ..)φメモメモ</p>
		</div>
	</div>
	<div class="return-area center">
		<div class="row">
			<div class="col-sm 6 center">
				<input class="returnBtnLarge" type="button" value="登録画面へ"
					onclick="location.href='EmployeeNewRegist'">
			</div>
			<div class="col-sm 6 center">
				<input class="returnBtnLarge" type="button" value="管理者画面へ"
					onclick="location.href='AdminMenu'">
			</div>
		</div>
	</div>
	<div class="footer"></div>
</body>
</html>