<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="Stylesheet" href="css/common.css">
<link rel="Stylesheet" href="css/employeeDeleteComplete.css">
<title>修正完了</title>
</head>
<body>
	<div class="header"></div>
	<div class="header-text-area center"></div>
	<div class="complete-form-area">
		<div class="row complete-form-area-line">
			<div class="col-sm center">
				<h2 class="style">修正完了('▽')ゞ！</h2>
			</div>
			<div class="col-sm center">
				<input class="returnBtnLarge" type="button" name="delete"
					value="管理者メニュー" onclick="location.href='AdminMenu'"> <input
					class="returnBtnLarge" type="button" name="delete" value="ログアウト"
					onclick="location.href='Logout'">
			</div>
		</div>
	</div>
	<div class="footer"></div>
</body>
</html>