<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="Stylesheet" href="css/common.css">
<link rel="Stylesheet" href="css/login.css">
<title>ログイン</title>
</head>
<body>
	<div class="header"></div>
	<div class="header-text-area">
	</div>
	<form action="Login" method="post">

		<div class="center login-form-area">
			<div class="row">
				<p class="errMsgStyle center">${errMsg}</p>
			</div>
			<div class="row login-form-area-line">
				<div class="col-sm-5">
					<h2>ログインID</h2>
				</div>
				<div class="col-sm-7">
					<input class="loginTb" type="text" name="empId">
				</div>
			</div>
			<div class="row login-form-area-line">
				<div class="col-sm-5">
					<h2>パスワード</h2>
				</div>
				<div class="col-sm-7">
					<input class="loginTb" type="password" name="password">
				</div>
			</div>
			<div class="row login-form-area-line">
				<div class="col-sm">
					<input class="loginBtn" type="submit" value="ログイン">
				</div>
			</div>
		</div>
	</form>
	<div class="footer"></div>
</body>
</html>