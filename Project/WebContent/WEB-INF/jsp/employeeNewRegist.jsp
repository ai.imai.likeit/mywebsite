<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="Stylesheet" href="css/common.css">
<link rel="Stylesheet" href="css/employeeNewRegist.css">
<title>新入社員登録</title>
</head>
<body>
	<div class="header"></div>
	<div class="center header-text-area">
		<div class="row">
			<div class="col-sm-5">${empLogin.loginDateStrJap}</div>
			<div class="col-sm-2">
				やっほー今日もお疲れ様<br> <span class="textBold">${empLogin.name}様</span>(*^^)旦★
			</div>
			<div class="col-sm-5">
				<a href="Logout">ログアウト</a>
			</div>
		</div>
	</div>
	<br>
	<form action="EmployeeNewRegist" method="post">
		<div class="center registraion-form-area">
			<div class="row">
				<div class="errMsgStyle center">${errMsg}</div>
			</div>
			<div class="row registraion-form-area-line">
				<div class="col-sm">
					<h3>社員登録</h3>
				</div>
			</div>
			<div class="row registraion-form-area-line">
				<div class="col-sm-2 right textBold">社員ID</div>
				<div class="col-sm-4 left">
					<input class="registrationTb" type="text" name="empId"
						value="${empBeans.empId}" placeholder="EE1000">
				</div>
				<div class="col-sm-2 right textBold">社員名</div>
				<div class="col-sm-4 left">
					<input class="registrationTb" type="text" name="empName"
						value="${empBeans.name}">
				</div>
			</div>
			<div class="row registraion-form-area-line">

				<div class="col-sm-2 right textBold">パスワード</div>
				<div class="col-sm-4 left">
					<input class="registrationTb" type="password" name="empPass">
				</div>

				<div class="col-sm-2 right textBold">住所</div>
				<div class="col-sm-4 left">
					<input class="registrationTb" type="text" name="empAdress"
						value="${empBeans.adress}" placeholder="東京都中央区日本橋">
				</div>
			</div>
			<div class="row registraion-form-area-line">
				<div class="col-sm-2 right textBold">部署名</div>
				<div class="col-sm-4 left">
					<select class="department" name="departId" size="1">
						<option value="">部署</option>
						<c:forEach var="d" items="${departList}">
							<c:if test="${d.id == empBeans.departmentId}">
								<option selected value="${d.id}">${d.name}</option>
							</c:if>
							<c:if test="${d.id != empBeans.departmentId}">
								<option value="${d.id}">${d.name}</option>
							</c:if>
						</c:forEach>
					</select>
				</div>
				<div class="col-sm-2 right textBold">電話番号</div>
				<div class="col-sm-4 left">
					<input class="registrationTb" type="text" name="empTel"
						value="${empBeans.tel}" placeholder="03-1234-5678">
				</div>
			</div>
			<div class="row registraion-form-area-line">

				<div class="col-sm-2 right textBold">入社年月日</div>
				<div class="col-sm-4 left">
					<input class="registrationTb" type="date" name="empEntryDate"
						value="${empBeans.entryDateStr}">
				</div>
				<div class="col-sm-2 right textBold">生年月日</div>
				<div class="col-sm-4 left">
					<input class="registrationTb" type="date" name="empBirthDate"
						value="${empBeans.birthStr}">
				</div>
			</div>
			<div class="row registraion-form-area-line">
				<div class="col-sm-2 right textBold"></div>
				<div class="col-sm-4 left"></div>
				<div class="col-sm-2"></div>
				<div class="col-sm-4"></div>
			</div>

			<div class="row registraion-form-area-line">
				<div class="col-sm">
					<input class="confirmBtn" type="submit" value="確認">
				</div>
			</div>
		</div>
		<div class="row center"></div>

		</div>
	</form>
	<div class="row">
		<div class="col-sm center">
			<input class="returnBtn" type="submit" value="戻る"
				onclick="location.href='AdminMenu'">
		</div>
	</div>
	<div class="footerLong"></div>
</body>
</html>