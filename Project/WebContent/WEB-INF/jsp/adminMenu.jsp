<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="Stylesheet" href="css/common.css">
<link rel="Stylesheet" href="css/adminMenu.css">
<title>管理者画面</title>
</head>
<body>
	<div class="header"></div>
	<div class="center header-text-area">
		<div class="row">
			<div class="col-sm-5">${empLogin.loginDateStrJap}</div>
			<div class="col-sm-2">
				やっほー今日もお疲れ様<br> <span class="textBold">${empLogin.name}様</span>(*^^)旦★
			</div>
			<div class="col-sm-5">
				<a href="Logout">ログアウト</a>
			</div>
		</div>
	</div>
	<br>
	<div class="newRegist-form-area">
		<div class="row">
			<div class="col-sm center">社員登録</div>
		</div>
		<div class="row">
			<div class="col-sm center">
				<input class="newRegistBtn" type="submit" name="newRegist"
					value="新規登録" onclick="location.href='EmployeeNewRegist'">
			</div>
		</div>
	</div>
	<br>
	<div class="search-form-area">
		<div class="row ">
			<div class="col-sm center">
				<h3>検索</h3>
			</div>
		</div>
		<form action="AdminMenu" method="post">
			<div class="row search-form-area-line ">
				<div class="col-sm-4 center">社員ID</div>
				<div class="col-sm-4 center">部署</div>
				<div class="col-sm-4 center">名前</div>
			</div>
			<div class="row">
				<div class="col-sm-4 center">
					<input class="textTb" type="text" name="empId">
				</div>
				<div class="col-sm-4 center">
					<select class="department" name="departId" size="1">
						<option value="">部署</option>
						<c:forEach var="d" items="${departList}">
							<option value="${d.id}">${d.name}</option>
						</c:forEach>
					</select>
				</div>
				<div class="col-sm-4 center">
					<input class="textTb" type="text" name="name">
				</div>
			</div>
			<div class="row search-form-area-line">
				<div class="col-sm center ">
					<input class="searchBtn" type="submit" name="search" value="検索">
				</div>
			</div>
		</form>
	</div>

	<div class="empList-area">
		<div class="row">
			<div class="col-sm-2">
				<h5>一覧</h5>
			</div>
		</div>
		<div class="row empList-heading">
			<div class="col-sm-2 center">社員ID</div>
			<div class="col-sm-2 center">部署</div>
			<div class="col-sm-2 center">名前</div>
			<div class="col-sm-1 center"></div>
			<div class="col-sm-1 center"></div>
		</div>

		<c:if test="${empty empList}">
			<div class="row empList-area-line">
				<div class="col-sm center">${Msg}</div>
			</div>
		</c:if>
		<c:if test="${!empty empList}">
			<c:forEach var="list" items="${empList}">
				<div class="row empList-area-line">
					<div class="col-sm-2 center">${list.empId}</div>
					<div class="col-sm-2 center">${list.depName}</div>
					<div class="col-sm-2 center">${list.name}</div>
					<div class="col-sm-1 center">
						<input class="updateBtn" type="button" name="update" value="修正"
							onclick="location.href='EmployeeUpdate?id=${list.id}'">
					</div>
					<div class="col-sm-1 center">
						<form action="">
							<input class="deleteBtn" type="button" name="delete" value="削除"
							onclick="location.href='EmployeeDelete?id=${list.id}'">
						</form>
					</div>
				</div>
			</c:forEach>
		</c:if>
		<div class="row empList-footing ">
			<div class="col-sm center"></div>
		</div>
	</div>
	<br>
	<div class="footerLong"></div>
</body>
</html>