<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="Stylesheet" href="css/common.css">
<link rel="Stylesheet" href="css/employeeDelete.css">
<title>削除</title>
</head>
<body>
	<div class="header"></div>
	<div class="center header-text-area">
		<div class="row">
			<div class="col-sm-5">${empLogin.loginDateStrJap}</div>
			<div class="col-sm-2">
				やっほー今日もお疲れ様<br> <span class="textBold">${empLogin.name}様</span>(*^^)旦★
			</div>
			<div class="col-sm-5">
				<a href="Logout">ログアウト</a>
			</div>
		</div>
	</div>
	<br>
	<form action="EmployeeDelete" method="post">
		<div class="empList-area">
			<div class="row">
				<div class="col-sm center">
					<h5>消して大丈夫(*･_･*)？</h5>
				</div>
			</div>
			<div class="row empList-heading">
				<div class="col-sm-4 center">社員ID</div>
				<div class="col-sm-4 center">部署</div>
				<div class="col-sm-4 center">名前</div>
			</div>
			<div class="row empList-area-line">
				<input type="hidden" name="id" value="${deleteEmp.id}">
				<div class="col-sm-4 center">${deleteEmp.empId}</div>
				<div class="col-sm-4 center">${deleteEmp.departmentName}</div>
				<div class="col-sm-4 center">${deleteEmp.name}</div>
			</div>
			<div class="row empList-footing ">
				<div class="col-sm center"></div>
			</div>
		</div>
		<div class="bottun-area">
			<div class="row">
				<div class="col-sm center">消して大丈夫！</div>
			</div>
			<div class="row bottun-area-line">
				<div class="col-sm center">
					<input class="returnBtn" type="submit" name="delete" value="削除">
				</div>
			</div>
		</div>
	</form>
	<div class="row">
		<div class="col-sm center">やっぱりやめる…</div>
	</div>
	<div class="row bottun-area-line">
		<div class="col-sm center">
			<input class="returnBtn" type="button" name="delete" value="戻る"
				onclick="location.href='AdminMenu'">
		</div>
	</div>
	</div>



	<div class="footer"></div>
</body>
</html>