<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="Stylesheet" href="css/common.css">
<link rel="Stylesheet" href="css/employeeUpdate.css">
<title>登録更新</title>
</head>
<body>
	<div class="header"></div>
	<div class="center header-text-area">
		<div class="row">
			<div class="col-sm-5">${empLogin.loginDateStrJap}</div>
			<div class="col-sm-2">
				やっほー今日もお疲れ様<br> <span class="textBold">${empLogin.name}様</span>(*^^)旦★
			</div>
			<div class="col-sm-5">
				<a href="Logout">ログアウト</a>
			</div>
		</div>
	</div>

	<form action="EmployeeUpdate" method="post">
		<div class="update-area">
			<div class="row update-area-headding">
				<div class="col-sm">
					<h3>さぁ修正だ(・ロ・)ノｶﾓﾝ！！</h3>
				</div>
			</div>
			<div class="row update-area-line">
				<div class="col-sm-4 center textBold">社員ID</div>
				<div class="col-sm-4 center textBold">社員名</div>
				<div class="col-sm-4 center textBold"></div>
			</div>
			<div class="row">
				<input type="hidden" name="id" value="${updateEmp.id}">
				<div class="col-sm-4 center">${updateEmp.empId}</div>
				<div class="col-sm-4 center">
					<input class="textTb" type="text" name="name" value="${updateEmp.name}">
				</div>
				<div class="col-sm-4 center"></div>
			</div>
			<div class="row update-area-line">
				<div class="col-sm-4 center textBold">年齢</div>
				<div class="col-sm-4 center textBold">住所</div>
				<div class="col-sm-4 center textBold">電話番号</div>
			</div>
			<div class="row">
				<div class="col-sm-4 center">${updateEmp.age}</div>
				<div class="col-sm-4 center">
					<input class="textTb" type="text" name="adress"
						value="${updateEmp.adress}">
				</div>
				<div class="col-sm-4 center">
					<input class="textTb" type="text" name="tel"
						value="${updateEmp.tel}">
				</div>
			</div>
			<div class="row update-area-line">
				<div class="col-sm-4 center textBold">所属部署</div>
				<div class="col-sm-4 center textBold">出勤時間</div>
				<div class="col-sm-4 center textBold">退勤時間</div>
			</div>
			<div class="row">
				<div class="col-sm-4 center">
					<select class="department" name="departId" size="1">
						<option value="">部署</option>
						<c:forEach var="d" items="${departList}">
							<c:if test="${d.id == updateEmp.departmentId}">
								<option selected value="${d.id}">${d.name}</option>
							</c:if>
							<c:if test="${d.id != updateEmp.departmentId}">
								<option value="${d.id}">${d.name}</option>
							</c:if>
						</c:forEach>
					</select>
				</div>
				<div class="col-sm-4 center">
					<input class="textTb" type="text" name="startWork" value="${updateEmp.startWork}">
				</div>
				<div class="col-sm-4 center">
					<input class="textTb" type="text" name="closeWork" value="${updateEmp.closeWork}">
				</div>
			</div>
			<div class="row">
				<div class="col-sm center ">
					<input class="updateBtn" type="submit" name="update" value="修正">
				</div>
			</div>
		</div>
	</form>
	<div class="footerLong"></div>
</body>
</html>