select
count(1)
from
attendance
where MONTH(days) >= 6
;

Insert into attendance(employee_id,days,dayofweek) values(?,?,?);

select id,empid_no,days,dayofweeks,kind,attend_time,start_time,lunch_time,close_time,over_time 
from attendance where empid_no =2  and MONTH(days) = 5 
order by id asc
;

update attendance set start_time = ? where empid_no = ? and days = ?;

select start_time as startTime from attendance where empid_no = ? and days = ?

update attendance 
set attend_time as attenTime,start_time as startTime,lunch_time as lunchTime,close_time as closeTime,over_time as overTime 
where empid_no = ? and days = ?;

select sum(attend_time) as attendTotalTime from attendance
where empid_no = 2 and MONTH(days) = 5;

select
    attend_time,
    time_to_sec(attend_time) as sec
from
    attendance
;

select
sum(time_to_sec(attend_time)) as sec,
sec_to_time(sum(time_to_sec(attend_time))) as totalSec
from attendance
where empid_no = 2 and MONTH(days) = 5;

select status from attendance 
where empid_no = 2 and days = '2019/05/01'
;
