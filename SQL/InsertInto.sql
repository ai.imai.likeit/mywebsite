INSERT INTO department(dep_name) 
VALUES('総務部')
;

INSERT INTO department(dep_name) 
VALUES('人事部')
;

INSERT INTO department(dep_name) 
VALUES('経理部')
;

INSERT INTO department(dep_name) 
VALUES('営業部')
;

INSERT INTO dayofweek(day_name) 
VALUES('月')
;
INSERT INTO dayofweek(day_name) 
VALUES('火')
;
INSERT INTO dayofweek(day_name) 
VALUES('水')
;
INSERT INTO dayofweek(day_name) 
VALUES('木')
;
INSERT INTO dayofweek(day_name) 
VALUES('金')
;
INSERT INTO dayofweek(day_name) 
VALUES('土')
;
INSERT INTO dayofweek(day_name) 
VALUES('日')
;
INSERT INTO employee 
(emp_id,password,name,birth,department,start_work,close_work,entry_date,registration_date) 
VALUES
('admin','abc000','admin','1990-01-01',2,'09:00:00','18:00:00','2019-04-01','2000-01-01 01:00');

DELETE FROM employee 
WHERE id = 1;

UPDATE employee SET emp_id = 'admin' 
WHERE id = 1;
