CREATE DATABASE 
mywebsite 
DEFAULT CHARACTER SET utf8;

CREATE TABLE 
employee 
(id SERIAL primary key,
 emp_id varchar(10) UNIQUE NOT NULL,
 password varchar(255) NOT NULL,
 name varchar(30) NOT NULL,
 age int(3) NOT NULL,
 adress varchar(100) NOT NULL,
 tel varchar(25) NOT NULL,
 birth DATE NOT NULL,
 department int NOT NULL,
 start_work TIME NOT NULL,
 close_work TIME NOT NULL,
 entry_date DATE NOT NULL,
 registration_date DATETIME NOT NULL,
 update_date DATETIME default '00-00-00 00:00:00',
 login_date DATETIME default '00-00-00 00:00:00',
 logout_date DATETIME default '00-00-00 00:00:00'
 ) ;
 
CREATE TABLE 
attendance 
(id SERIAL primary key,
 empid_no int NOT NULL,
 days DATE NOT NULL,
 dayofweeks varchar(5) NOT NULL,
 kind varchar(10),
 attend_time TIME default '00:00:00',
 start_time TIME default '00:00:00',
 lunch_time TIME default '00:00:00',
 end_time TIME default '00:00:00',
 over_time TIME default '00:00:00',
 status int(1) NOT NULL default '0'
 ) ;
 
CREATE TABLE 
department 
(dep_id SERIAL primary key,
 dep_name varchar(50) UNIQUE NOT NULL
 );
 
create table sample(
	sample_date Date,
	sample_time Time
)

insert sample(sample_date,sample_time) values('2015-08-08','23:00:00')
insert sample(sample_date,sample_time) values('2015-09-08','01:00:00');
insert sample(sample_date,sample_time) values('2015-10-08','02:00:00');
insert sample(sample_date,sample_time) values('2015-11-08','24:00:00');

select
*
from
sample
order by
sample_time

delete from attendance;

DROP TABLE employee;

insert sample(sample_date,sample_time) values('Fri May 24 00:00:00 JST 1974','Fri May 24 00:00:00 JST 1974);

alter table employee add login_date TIMESTAMP NULL;

